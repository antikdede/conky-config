My Conky configuration

<img src='conky-screenshot.png' width='455px'>

copy fonts folder content into ~/.fonts or relevant fonts folder and run "`fc-cache -f -v`"

run `chmod -R +x scripts` and copy scripts folder content to /usr/local/bin or other custom bin folder in PATH

Mandatory changes:
```
conkyrc: lua_load="~/CHANGEPATH/bargraph.lua",
```
Optional changes (or just remove externip.sh\*, checkupdates** and checkgmail\*** lines in conkyrc if you don't plan to use):
```
conkyrc:${texeci 450 ~/CHANGEPATH/externip.sh}${color}
scripts/checkupdates: tgu="user#CHANGEME"
scripts/checkupdates: tgu="user#CHANGEME"
scripts/checkupdates: echo "${pkg}" >> /tmp/tgsent.txt; telegram-cli -k ~/CHANGEPATH/tg-server.pub -WDR -e "msg ${tgu} Update: ${pkg}" &> /dev/null &
scripts/checkupdates:if [[ -e ~/CHANGEPATH/importantpkgs.txt ]]; then
scripts/checkupdates: readarray importantpkgs < ~/CHANGEPATH/importantpkgs.txt # put important pkg names one per line in this file
scripts/gmail_conky.py:username = 'CHANGEME@gmail.com'
scripts/gmail_conky.py:password = 'CHANGEME'
scripts/checkgmail:python2 ~/CHANGEPATH/gmail_conky.py
```
\* uses curl but if you prefer the alternative dig (opendns) method, modify lines in file.

\*\* for Arch Linux

\*\*\* requires allowing less secure apps on https://myaccount.google.com/lesssecureapps
